package easydht

import "testing"

func TestLeadingZeroLen(t *testing.T) {
	cases := []struct {
		in  []byte
		out int
	}{
		{[]byte{0, 0, 255}, 16},        // 00000000 00000000 11111111
		{[]byte{0, 1, 254}, 15},        // 00000000 00000001 11111110
		{[]byte{0, 0, 0, 0, 0, 2}, 46}, // 00000000 00000000 00000000 00000000 00000000 00000010
	}
	for _, tt := range cases {
		actual := LeadingZeroBitsLen(string(tt.in))
		if tt.out != actual {
			t.Errorf("%d leading zeros bits expected for bytes sequence %b, but %d given", tt.out, tt.in, actual)
		}
	}
}

func TestXOR(t *testing.T) {
	cases := []struct {
		in  [2][]byte
		out []byte
	}{
		{
			[2][]byte{
				[]byte{2, 9, 4}, // 00000010 00001001 00000100
				[]byte{2, 4, 4}, // 00000010 00000100 00000100
			},
			[]byte{0, 13, 0}, // 00000000 00001101 00000000
		},
	}
	for _, tt := range cases {
		actual := XOR(string(tt.in[0]), string(tt.in[1]))
		if string(tt.out) != actual {
			t.Errorf("%b bytes sequence expected, but %b given", tt.out, []byte(actual))
		}
	}
}
