package easydht

import (
	"encoding/json"
	"errors"
	"net"
	"sync"
	"time"
)

const (
	messageTypeRequest = iota
	messageTypeResponse
	messageTypeError
)

const (
	rpcMethodPing     = "PING"
	rpcMethodFindNode = "FIND_NODE"
)

type remoteNodeResponse struct {
	Sender   *Contact
	Contacts []*Contact
}

type RPCHandler struct {
	socket           *net.UDPConn
	responseNotifier responseNotifier
	routingTable     *RoutingTable
}

type responseNotifier struct {
	subscriptions map[int]chan *remoteNodeResponse
	m             sync.RWMutex
}

type responseSubscription struct {
	messageID int
	channel   chan *remoteNodeResponse
}

func (r *responseNotifier) subscribe(rs *responseSubscription) {
	delay := time.Duration(time.Second * 1)
	r.m.Lock()
	r.subscriptions[rs.messageID] = rs.channel
	r.m.Unlock()
	go r.unsubscribe(rs.messageID, &delay)
}

func (r *responseNotifier) channel(messageID int) (chan *remoteNodeResponse, bool) {
	r.m.Lock()
	val, ok := r.subscriptions[messageID]
	r.m.Unlock()
	return val, ok
}

func (r *responseNotifier) unsubscribe(messageID int, delay *time.Duration) {
	if delay != nil {
		time.Sleep(*delay)
	}
	r.m.Lock()
	_, ok := r.subscriptions[messageID]
	if ok {
		delete(r.subscriptions, messageID)
	}
	r.m.Unlock()
}

type RPCErrorMessage struct {
	Error string
}

type FindNodeRequest struct {
	TargetID string
}

type FindNodeResponse struct {
	Contacts []*Contact
}

type RPCMessage struct {
	Type      int
	Method    string
	MessageID int
	SenderID  string
	Data      *json.RawMessage
}

func decodeContactsIDs(contacts []*Contact, keyLength int) error {
	var err error
	for _, contact := range contacts {
		contact.ID, err = decodeID(contact.ID, keyLength)
		if err != nil {
			return err
		}
	}
	return nil
}

func parseRemoteContact(address *net.UDPAddr, senderID string, keyLength int) (*Contact, error) {
	host, port, err := splitHostAndPort(address.String())
	if err != nil {
		return nil, err
	}

	decodedSenderID, err := decodeID(senderID, keyLength)
	if err != nil {
		return nil, err
	}

	return &Contact{decodedSenderID, host, port}, nil
}

func (h *RPCHandler) handleRPCRequest(conn *net.UDPConn, data []byte, address *net.UDPAddr) {
	incomingMessage := &RPCMessage{}
	err := json.Unmarshal(data, &incomingMessage)
	if err != nil {
		return
	}

	remoteContact, err := parseRemoteContact(address, incomingMessage.SenderID, h.routingTable.keyLength)

	if incomingMessage.Type == messageTypeRequest {
		responseMessage := RPCMessage{}
		responseMessage.Type = messageTypeResponse

		var responseMessageData interface{}
		var rpcError error

		switch incomingMessage.Method {
		case rpcMethodPing:
			responseMessage.Method = rpcMethodPing
		case rpcMethodFindNode:
			responseMessage.Method = rpcMethodFindNode
			findNodeRequest := FindNodeRequest{}
			err := json.Unmarshal([]byte(*incomingMessage.Data), &findNodeRequest)
			if err != nil {
				rpcError = err
				break
			}
			responseMessageData, rpcError = h.HandleFindNode(&findNodeRequest, remoteContact)
		default:
			responseMessage.Type = 2
			rpcError = errors.New("method does not exists")
			return
		}

		if rpcError != nil {
			responseMessageData = RPCErrorMessage{rpcError.Error()}
		} else {
			h.routingTable.Update(remoteContact)
		}

		data, _ := json.Marshal(responseMessageData)
		rawData := json.RawMessage(data)

		responseMessage.SenderID = h.routingTable.self.IDHex()
		responseMessage.Data = &rawData
		responseMessage.MessageID = incomingMessage.MessageID

		message, _ := json.Marshal(responseMessage)
		conn.WriteToUDP(message, address)
	} else if incomingMessage.Type == messageTypeResponse {
		ch, ok := h.responseNotifier.channel(incomingMessage.MessageID)
		if !ok {
			return
		}

		switch incomingMessage.Method {
		case rpcMethodPing:
			ch <- &remoteNodeResponse{remoteContact, []*Contact{}}
			h.responseNotifier.unsubscribe(incomingMessage.MessageID, nil)
		case rpcMethodFindNode:
			if incomingMessage.Type == 1 {
				findNodeResponse := FindNodeResponse{}
				err := json.Unmarshal([]byte(*incomingMessage.Data), &findNodeResponse)
				if err != nil {
					return
				}

				err = decodeContactsIDs(findNodeResponse.Contacts, h.routingTable.keyLength)
				if err != nil {
					return
				}

				h.routingTable.Update(remoteContact)
				ch <- &remoteNodeResponse{remoteContact, findNodeResponse.Contacts}
				h.responseNotifier.unsubscribe(incomingMessage.MessageID, nil)
			}
		}

		h.routingTable.Update(remoteContact)
	} else if incomingMessage.Type == messageTypeError {
		// todo: log
	}
}

func NewRPCHandler(rt *RoutingTable) (*RPCHandler, error) {
	handler := new(RPCHandler)
	address, err := net.ResolveUDPAddr("udp", rt.self.Address())
	if err != nil {
		return handler, err
	}

	conn, err := net.ListenUDP("udp", address)
	if err != nil {
		return handler, err
	}
	handler.socket = conn
	handler.routingTable = rt
	handler.responseNotifier = responseNotifier{map[int]chan *remoteNodeResponse{}, sync.RWMutex{}}

	go func() {
		for {
			buf := make([]byte, 1024)
			n, address, err := conn.ReadFromUDP(buf)
			if err != nil {
				continue
			}

			go handler.handleRPCRequest(conn, buf[:n], address)
		}

		conn.Close()
	}()

	return handler, err
}

func (h *RPCHandler) HandleFindNode(r *FindNodeRequest, remoteContact *Contact) (*FindNodeResponse, error) {
	targetID, err := decodeID(r.TargetID, h.routingTable.keyLength)
	if err != nil {
		return nil, err
	}

	closestContacts := h.routingTable.FindClosest(targetID, 20)
	contacts := []*Contact{}
	for _, contact := range closestContacts {
		contacts = append(contacts, &Contact{
			ID:   contact.Contact.IDHex(),
			Host: contact.Contact.Host,
			Port: contact.Contact.Port,
		})
	}
	return &FindNodeResponse{contacts}, nil
}

func (h *RPCHandler) Ping(address string, ch *chan *remoteNodeResponse) error {
	messageID := randInt()
	jsonMessage, _ := json.Marshal(RPCMessage{
		Method:    rpcMethodPing,
		MessageID: messageID,
		SenderID:  h.routingTable.self.IDHex(),
	})
	var subscription *responseSubscription
	if ch != nil {
		subscription = &responseSubscription{messageID, *ch}
	}
	return h.sendMessage(address, jsonMessage, subscription)
}

func (h *RPCHandler) FindNode(id, address string, ch *chan *remoteNodeResponse) error {
	messageID := randInt()
	encodedID := encodeID(id)
	request := FindNodeRequest{encodedID}
	rawJson, err := toRawJson(request)
	if err != nil {
		return err
	}
	message := RPCMessage{
		Method:    rpcMethodFindNode,
		MessageID: messageID,
		SenderID:  h.routingTable.self.IDHex(),
		Data:      &rawJson,
	}
	jsonMessage, err := json.Marshal(&message)
	if err != nil {
		return err
	}
	var subscription *responseSubscription
	if ch != nil {
		subscription = &responseSubscription{messageID, *ch}
	}
	return h.sendMessage(address, jsonMessage, subscription)
}

func (h *RPCHandler) IterativeFindNode(id string) ([]*sortableContact, error) {
	shortlist := []*sortableContact{}
	shortlist = append(shortlist, h.routingTable.FindClosest(id, 40)...)
	ch := make(chan *remoteNodeResponse)
	queriedNodes := map[string]bool{}

	for _, c := range shortlist {
		h.FindNode(id, c.Contact.Address(), &ch)
	}

	for {
		select {
		case response := <-ch:
			_, ok := queriedNodes[response.Sender.ID]
			if ok {
				break
			}
			queriedNodes[response.Sender.ID] = true
			for _, c := range response.Contacts {
				_, ok := queriedNodes[c.ID]
				if ok {
					continue
				}
				if c.ID == h.routingTable.self.ID {
					continue
				}
				h.FindNode(id, c.Address(), &ch)
			}
			if h.routingTable.self.ID == response.Sender.ID {
				break
			}

			shortlist = addContact(shortlist, newSortableContact(response.Sender, id))
		case <-time.After(time.Second * 1):
			break
		}
		if len(h.responseNotifier.subscriptions) == 0 {
			break
		}
	}

	return shortlist, nil
}

func (h *RPCHandler) sendMessage(address string, data []byte, subscription *responseSubscription) error {
	udpAddress, err := net.ResolveUDPAddr("udp", address)
	if err != nil {
		return err
	}
	_, err = h.socket.WriteToUDP(data, udpAddress)
	if err != nil {
		return err
	}
	if subscription != nil {
		h.responseNotifier.subscribe(subscription)
	}

	return nil
}
