package easydht

import (
	"strconv"
	"net"
	"math/rand"
	"encoding/json"
)

func splitHostAndPort(address string) (string, int, error) {
	host, port, err := net.SplitHostPort(address)
	if err != nil {
		return "", 0, err
	}
	p, _ := strconv.Atoi(port)

	return host, p, err
}

func randInt() int {
	return rand.Intn(4294967296)
}

func toRawJson(data interface{}) (json.RawMessage, error) {
	jsonData, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}
	return json.RawMessage(jsonData), nil
}